<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLicenseApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('license_applications', function (Blueprint $table){
            $table->increments('id');
            $table->integer('business_id')->unsigned();
            $table->integer('license_id')->unsigned();
            $table->integer('duration');//in months
            $table->integer('status')->default(0);//default pending
            $table->integer('action_by')->default(0);//who altered status last
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('business_id')->references('id')->on('businesses');
            $table->foreign('license_id')->references('id')->on('licenses');
            //$table->foreign('action_by')->references('id')->on('users');
            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('license_applications');
    }
}
