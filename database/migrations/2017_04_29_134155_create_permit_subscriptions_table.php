<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermitSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permit_subscriptions', function (Blueprint $table){
            $table->increments('id');
            $table->integer('business_id')->unsigned();
            $table->integer('permit_id')->unsigned();
            $table->float('fee');
            $table->dateTime('commencement');
            $table->dateTime('expiry');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('business_id')->references('id')->on('businesses');
            $table->foreign('permit_id')->references('id')->on('permits');
            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permit_subscriptions');
    }
}
