<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBusinessRegistrationApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('business_registration_applications', function (Blueprint $table){
            $table->increments('id');
            $table->string('name');
            $table->string('phone_a');
            $table->string('phone_b');
            $table->string('email')->unique();
            $table->string('location');
            //coordinates
            $table->decimal('lat', 10, 7);
            $table->decimal('lng', 10, 7);
            //sha1
            $table->string('description');
            $table->integer('user_id')->unsigned();//owner of biz
            $table->integer('industry_id')->unsigned();
            $table->integer('status')->default(0);//default pending
            $table->integer('action_by')->default(0);//who altered status last
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('industry_id')->references('id')->on('industries');
            //$table->foreign('action_by')->references('id')->on('users');
            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('business_registration_applications');
    }
}
