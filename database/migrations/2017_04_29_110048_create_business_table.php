<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBusinessTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('businesses', function (Blueprint $table){
           $table->increments('id');
           $table->string('name');
           $table->string('phone_a');
           $table->string('phone_b');
           $table->string('email')->unique();
           $table->string('location');
           //coordinates
            $table->decimal('lat', 10, 7);
           $table->decimal('lng', 10, 7);
           $table->string('description');
            $table->integer('industry_id')->unsigned();
           $table->integer('user_id')->unsigned();
           $table->timestamps();
           $table->softDeletes();
           $table->foreign('industry_id')->references('id')->on('industries');
            $table->foreign('user_id')->references('id')->on('users');
            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('businesses');
    }
}
