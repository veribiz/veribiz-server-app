<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create(
            [
                'name' => 'Mutai Mwiti',
                'email' => 'mutaimwiti40@gmail.com',
                'password' => bcrypt('pass'),
                'role' => 0
            ]
        );

        User::create(
            [
                'name' => 'Dell Vostro',
                'email' => 'dellvostrox@gmail.com',
                'password' => bcrypt('pass'),
                'role' => 1
            ]
        );

        User::create(
            [
                'name' => 'Antonio Valencia',
                'email' => 'avalencia@gmail.com',
                'password' => bcrypt('pass'),
                'role' => 1
            ]
        );
    }
}
