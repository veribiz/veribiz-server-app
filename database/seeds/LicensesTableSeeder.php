<?php

use Illuminate\Database\Seeder;
use App\License;

class LicensesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        License::create(
            [
                'name' => 'Simple business license',
                'description' => 'Most businesses will fall under this category.',
                'fee' => 10000
            ]
        );

        License::create(
            [
                'name' => 'Firearms business license',
                'description' => 'Any business dealing with firearms must obtain this license.',
                'fee' => 50000
            ]
        );
    }
}
