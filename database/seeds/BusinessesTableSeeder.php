<?php

use Illuminate\Database\Seeder;
use App\Business;
use App\Industry;

class BusinessesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Business::create(
            [
                'name' => 'Novack Entreprises',
                'phone_a' => '0755542443',
                'phone_b' => '0751298722',
                'email' => 'novackent@gamil.com',
                'location' => 'Meru Town',
                'lat' => '0.0499998',
                'lng' => '37.6499974',
                'description' => 'Dealers in all types of hardware equipment.',
                'industry_id' => Industry::where('name', 'Infrastructure and Energy')->first()->id,
                'user_id' => 2
            ]
        );
        Business::create(
            [
                'name' => 'Rexi animal care',
                'phone_a' => '0756952524',
                'phone_b' => '0768855423',
                'email' => 'rexianimalcare@gamil.com',
                'location' => 'Nkubu Town',
                'lat' => '-0.06466',
                'lng' => '37.66793',
                'description' => 'Suppliers of all livestock feeds.',
                'industry_id' => Industry::where('name', 'Livestock')->first()->id,
                'user_id' => 2
            ]
        );
    }
}
