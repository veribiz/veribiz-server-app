<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(IndustriesTableSeeder::class);
        $this->call(BusinessesTableSeeder::class);
        $this->call(LicensesTableSeeder::class);
        $this->call(PermitsTableSeeder::class);
    }
}
