<?php

use Illuminate\Database\Seeder;
use App\Industry;

class IndustriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
         * Agriculture
         * Water
         * Livestock
         * Environment
         * Youth and sports
         * Cooperatives
         * ICT
         * Education and Technology
         * Health
         * Infrastructure and Energy
         * Tourism
         * Land Use
         * Economic Planning
         * Industry and Trade
         */
        Industry::create(
            [
                'name' => 'Agriculture',
                'description' => 'Businesses that provide agricultural services.',
            ]
        );

        Industry::create(
            [
                'name' => 'Water',
                'description' => 'Businesses that provide water services.',
            ]
        );

        Industry::create(
            [
                'name' => 'Livestock',
                'description' => 'Businesses that provide livestock services.',
            ]
        );

        Industry::create(
            [
                'name' => 'Environment',
                'description' => 'Businesses that provide environmental services.',
            ]
        );

        Industry::create(
            [
                'name' => 'Youth and sports',
                'description' => 'Businesses that provide youth and sports services.',
            ]
        );

        Industry::create(
            [
                'name' => 'Cooperatives',
                'description' => 'Businesses that provide cooperative services.',
            ]
        );


        Industry::create(
            [
                'name' => 'ICT',
                'description' => 'Businesses that provide ICT services.',
            ]
        );


        Industry::create(
            [
                'name' => 'Education and Technology',
                'description' => 'Businesses that provide education and technology services.',
            ]
        );

        Industry::create(
            [
                'name' => 'Health',
                'description' => 'Businesses that provide health services.',
            ]
        );

        Industry::create(
            [
                'name' => 'Infrastructure and Energy',
                'description' => 'Businesses that provide infrastructure and energy services.',
            ]
        );

        Industry::create(
            [
                'name' => 'Tourism',
                'description' => 'Businesses that provide tourism services.',
            ]
        );

        Industry::create(
            [
                'name' => 'Land Use',
                'description' => 'Businesses that provide land use services.',
            ]
        );

        Industry::create(
            [
                'name' => 'Economic Planning',
                'description' => 'Businesses that provide economic planning services.',
            ]
        );

        Industry::create(
            [
                'name' => 'Industry and Trade',
                'description' => 'Businesses that provide industry and trade services.',
            ]
        );
    }
}
