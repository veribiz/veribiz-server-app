<?php

use Illuminate\Database\Seeder;
use App\Permit;

class PermitsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permit::create(
            [
                'name' => 'Building permit',
                'description' => 'Every business planning on building must obtain this.',
                'fee' => 2000
            ]
        );

        Permit::create(
            [
                'name' => 'Health department permit',
                'description' => 'Required for all businesses that deal with processing and or selling of food.',
                'fee' => 1000
            ]
        );
    }
}
