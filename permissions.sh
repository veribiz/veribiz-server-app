#! /bin/sh
sudo chmod 755 -R ./
sudo chown `whoami` -R ./
sudo chgrp `whoami` -R ./
sudo chmod 777 -R storage/
sudo chmod 777 -R bootstrap/cache/
