<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('login');
});

Auth::routes();

Route::get('/home', 'HomeController@home')->name('home');

Route::any('/business','BusinessController@index')->name('business');

Route::any('/license', 'LicenseController@index')->name('license');

Route::any('/permit', 'PermitController@index')->name('permit');

Route::any('/industry', 'IndustryController@index')->name('industry');

Route::any('/print', 'PrintController@index')->name('print');

Route::group(['prefix' => 'admin'], function () {
    Route::get('/', 'HomeController@admin')->name('admin');

    Route::any('/business','BusinessController@admin')->name('business');

    Route::any('/license', 'LicenseController@admin')->name('license');

    Route::any('/permit', 'PermitController@admin')->name('permit');

    Route::any('/industry', 'IndustryController@admin')->name('industry');

    Route::any('/print', 'PrintController@admin')->name('print');
});

//api
Route::get('/api/all', 'BusinessController@apiAll');