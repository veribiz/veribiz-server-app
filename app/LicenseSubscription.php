<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LicenseSubscription extends Model
{
    public $timestamps = true;
    protected $guarded = ['id'];
    protected $hidden = ['created_at', 'updated_at'];

    public function license()
    {
        return $this->belongsTo('App\License');
    }

    public function business()
    {
        return $this->belongsTo('App\Business');
    }
}
