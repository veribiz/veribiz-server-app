<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BusinessRegistrationApplication extends Model
{
    use SoftDeletes;

    public $timestamps = true;
    protected $guarded = ['id'];
    protected $hidden = ['created_at', 'updated_at'];
    protected $table = 'business_registration_applications';

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function industry()
    {
        return $this->belongsTo('App\Industry');
    }
}
