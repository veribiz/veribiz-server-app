<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permit extends Model
{
    public $timestamps = true;
    protected $guarded = ['id'];
    protected $hidden = ['created_at', 'updated_at'];
}
