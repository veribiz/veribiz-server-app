<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LicenseApplication extends Model
{
    public $timestamps = true;
    protected $guarded = ['id'];
    protected $hidden = ['created_at', 'updated_at'];

    public function business()
    {
        return $this->belongsTo('App\Business');
    }

    public function license()
    {
        return $this->belongsTo('App\License');
    }
}
