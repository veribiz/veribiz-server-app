<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PermitSubscription extends Model
{
    public $timestamps = true;
    protected $guarded = ['id'];
    protected $hidden = ['created_at', 'updated_at'];

    public function permit()
    {
        return $this->belongsTo('App\Permit');
    }

    public function business()
    {
        return $this->belongsTo('App\Business');
    }
}
