<?php

namespace App\Http\Controllers;
use App\Business;
use App\Permit;
use App\PermitApplication;
use App\PermitSubscription;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class PermitController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin')->only('admin');
        $this->middleware('auth')->only('index');
    }

    public function index()
    {
        switch (request('q')) {
            case 'subscriptions':
                return $this->viewSubscriptions();
            case 'permits':
                return $this->viewPermits();
            case 'apply':
                return $this->apply();
            case 'pending':
                return $this->viewPending();
            case 'approved':
                return $this->viewApproved();
            case 'rejected':
                return $this->viewRejected();
            case 'cancelled':
                return $this->viewCancelled();
            case 'cancel':
                return $this->cancelApplication();
            default:
                return back();
        }
    }

    public function admin()
    {
        switch (request('q')) {
            case 'add':
                return $this->addPermit();
            case 'permits':
                return $this->adminViewPermits();
            case 'subscriptions':
                return $this->adminViewSubscriptions();
            case 'pending':
                return $this->adminViewPending();
            case 'approved':
                return $this->adminViewApproved();
            case 'rejected':
                return $this->adminViewRejected();
            case 'cancelled':
                return $this->adminViewCancelled();
            case 'approve':
                return $this->approveApplication();
            case 'reject':
                return $this->rejectApplication();
            case 'delete':
                return $this->delete();
            default:
                return back();
        }
    }

    //simple user functions
    private function viewSubscriptions()
    {
        if(empty(request('id')))
        {
            if(!$this->hasBusinesses())
            {
                //no businesses
                return view('app.permit.subscriptions', ["business" => null]);
            }
            else
            {
                //get first business
                $business_id = $this->getBusinesses()[0]->id;
            }
        }
        else
        {
            $business_id = Business::find(request('id'))->id;
        }
        //get business permits
        $subscriptions = $this->getPermitsSubscriptions($business_id);
        return view('app.permit.subscriptions',
            [
                "business" => $business_id,
                "businesses" => $this->getBusinesses(),
                "subscriptions" => $subscriptions
            ]
        );
    }

    private function viewPermits()
    {
        if ($this->get()) {
            $permits = Permit::paginate(9);
            return view('app.permit.permits', compact('permits'));
        }
    }

    private function apply()
    {
        if ($this->get()) {
            return view('app.permit.application',
                [
                    "businesses" => $this->getBusinesses(),
                    "permits" => $this->getPermits()
                ]
            );
        }
        if ($this->post()) {
            PermitApplication::create(
                [
                    'business_id' => request('business'),
                    'permit_id' => request('permit'),
                    'duration' => request('duration')
                ]
            );
            $msg = "Made application for business permit.";
            return view('app.permit.application',
                [
                    "msg" => $msg,
                    "businesses" => $this->getBusinesses(),
                    "permits" => $this->getPermits()
                ]
            );
        }
    }

    private function viewPending()
    {
        if ($this->get()) {
            $pendingApps = PermitApplication::whereHas('business', function ($query){
                $query->where('user_id', Auth::user()->id);
            })
                ->where(['status' => 0])->paginate(9);
            return view('app.permit.pending', ['applications' => $pendingApps]);
        }
    }

    private function viewApproved()
    {
        if ($this->get()) {
            $approvedApps = PermitApplication::whereHas('business', function ($query){
                $query->where('user_id', Auth::user()->id);
            })
                ->where(['status' => 1])->paginate(9);
            return view('app.permit.approved', ['applications' => $approvedApps]);
        }
    }

    private function viewRejected()
    {
        if ($this->get()) {
            $rejectedApps = PermitApplication::whereHas('business', function ($query){
                $query->where('user_id', Auth::user()->id);
            })
                ->where(['status' => 2])->paginate(9);
            return view('app.permit.rejected', ['applications' => $rejectedApps]);
        }
    }

    private function viewCancelled()
    {
        if ($this->get()) {
            $cancelledApps = PermitApplication::whereHas('business', function ($query){
                $query->where('user_id', Auth::user()->id);
            })
                ->where(['status' => 3])->paginate(9);
            return view('app.permit.cancelled', ['applications' => $cancelledApps]);
        }
    }

    private function cancelApplication()
    {
        if ($this->get()){
            $appId = request('id');
            $application = PermitApplication::find($appId);
            //check if app belongs to user first
            if($application->business->user_id == Auth::user()->id)
            {
                //mark application as cancelled
                $application->status = 3;
                $application->action_by = Auth::user()->id;
                $application->save();
                $msg = "Cancelled permit application";
                return redirect()->back()->with('msg', $msg);
            }
        }
        return redirect()->back();
    }

    //admin functions
    private function addPermit()
    {
        if ($this->get())
        {
            return view('admin.permit.add');
        }
        if ($this->post())
        {
            Permit::create(
                [
                    "name" => request('name'),
                    "fee" => request('fee'),
                    "description" => request('description')
                ]
            );
            $msg = "Added permit.";
            return view('admin.permit.add', ["msg" => $msg]);
        }
    }

    private function adminViewPermits()
    {
        if ($this->get()) {
            $permits = Permit::paginate(9);
            return view('admin.permit.permits', compact('permits'));
        }
        if ($this->post())
        {
            $permit = Permit::find(request('permit'));
            $permit->name = request('name');
            $permit->fee = request('fee');
            $permit->description = request('description');
            $permit->save();
            $msg = "Edited permit";
            return view('admin.permit.permits',
                ["msg" => $msg, 'permits' => Permit::paginate(9)]
            );
        }
    }

    private function adminViewSubscriptions()
    {
        $subscriptions = PermitSubscription::paginate(9);
        return view('admin.permit.subscriptions', ['subscriptions' => $subscriptions]);
    }

    private function adminViewPending()
    {
        $applications = PermitApplication::where(['status' => 0])->paginate(9);
        return view('admin.permit.pending',
            ['applications' => $applications]
        );
    }

    private function adminViewApproved()
    {
        $applications = PermitApplication::where(['status' => 1])->paginate(9);
        return view('admin.permit.approved',
            ['applications' => $applications]
        );
    }

    private function adminViewRejected()
    {
        $applications = PermitApplication::where(['status' => 2])->paginate(9);
        return view('admin.permit.rejected',
            ['applications' => $applications]
        );
    }

    private function adminViewCancelled()
    {
        $applications = PermitApplication::where(['status' => 3])->paginate(9);
        return view('admin.permit.cancelled',
            ['applications' => $applications]
        );
    }

    private function approveApplication()
    {
        if ($this->get())
        {
            $appId = request('id');
            $application = PermitApplication::find($appId);
            //create permit subscription
            /*
             * issues to consider:
             * -duration: how to apply it to subscription
             * -duplicate subscription
             * commencement: now
             * expiry: now + duration
            */
            $date = Carbon::now();
            $commencement = $date->toDateTimeString();
            $expiry = $date->addMonths($application->duration)->toDateTimeString();
            PermitSubscription::create(
                [
                    'business_id' => $application->business_id,
                    'permit_id' => $application->permit_id,
                    'fee' => $application->permit->fee,
                    'commencement' => $commencement,
                    'expiry' => $expiry
                ]
            );
            //mark application as approved
            $application->status = 1;
            $application->action_by = Auth::user()->id;
            $application->save();
            $msg = "Approved permit application";
            return redirect()->back()->with('msg', $msg);
        }
        return redirect()->back();
    }

    private function rejectApplication()
    {
        if ($this->get()){
            $appId = request('id');
            $application = PermitApplication::find($appId);
            //mark application as rejected
            $application->status = 2;
            $application->action_by = Auth::user()->id;
            $application->save();
            $msg = "Rejected permit application";
            return redirect()->back()->with('msg', $msg);
        }
        return redirect()->back();
    }

    private function delete()
    {
        if ($this->get()){
            $permitId = request('id');
            //check if there are subscriptions, applications
            $subscriptions = PermitSubscription::where('permit_id', $permitId)->first();
            $applications = PermitApplication::where('permit_id', $permitId)->first();
            if (count($subscriptions) > 0 || count($applications) > 0)
            {
                //there are subscriptions. Abort
                $fail = "The permit cannot be deleted as there are subscriptions that rely on it";
                return redirect()->back()->with('fail', $fail);
            }
            else
            {
                //no subscriptions
                Permit::find($permitId)->delete();
                $msg = "Deleted permit";
                return redirect()->back()->with('msg', $msg);
            }
        }
        return redirect()->back();
    }

    //utility functions
    private function hasBusinesses(){
        $businesses = Business::where('user_id', Auth::user()->id)->get();
        if (count($businesses)) {
            return true;
        }
        else {
            return false;
        }
    }

    private function getBusinesses(){
        return Business::where('user_id', Auth::user()->id)->get();
    }

    private function getPermitsSubscriptions($business){
        return PermitSubscription::where('business_id', $business)->paginate(8);
    }

    private function getPermits(){
        return Permit::all();
    }
}
