<?php

namespace App\Http\Controllers;

use App\Business;
use App\Industry;
use App\License;
use App\LicenseSubscription;
use App\Permit;
use App\PermitSubscription;
use Illuminate\Support\Facades\Auth;

class PrintController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin')->only('admin');
        $this->middleware('auth')->only('index');
    }

    public function index()
    {
        switch (request('q')) {
            case 'licenses':
                return $this->printLicenses();
            case 'permits':
                return $this->printPermits();
            case 'industries':
                return $this->printIndustries();
            case 'registered':
                return $this->printRegistered();
            case 'license_subs':
                return $this->printLicenseSubs();
            case 'permit_subs':
                return $this->printPermitSubs();
            default:
                return back();
        }
    }

    public function admin()
    {
        switch (request('q')) {
            case 'licenses':
                return $this->adminPrintLicenses();
            case 'permits':
                return $this->adminPrintPermits();
            case 'industries':
                return $this->adminPrintIndustries();
            case 'registered':
                return $this->adminPrintRegistered();
            case 'license_subs':
                return $this->adminPrintLicenseSubs();
            case 'permit_subs':
                return $this->adminPrintPermitSubs();
            default:
                return back();
        }
    }

    //ordinary user functions
    private function printLicenses()
    {
        $licenses = License::all();
        return view('app.print.licenses', compact('licenses'));
    }

    private function printPermits()
    {
        $permits = Permit::all();
        return view('app.print.permits', compact('permits'));
    }

    private function printIndustries()
    {
        $industries = Industry::all();
        return view('app.print.industries', compact('industries'));
    }

    private function printRegistered()
    {
        $businesses = Business::where('user_id', Auth::user()->id)->get();
        return view('app.print.registered', compact('businesses'));
    }

    private function printLicenseSubs()
    {
        $subscriptions = LicenseSubscription::whereHas('business', function ($query){
            $query->where('user_id', Auth::user()->id);
        })->get();
        return view('app.print.license_subs', compact('subscriptions'));
    }

    private function printPermitSubs()
    {
        $subscriptions = PermitSubscription::whereHas('business', function ($query){
            $query->where('user_id', Auth::user()->id);
        })->get();
        return view('app.print.permit_subs', compact('subscriptions'));
    }

    //admin functions
    private function adminPrintLicenses()
    {
        $licenses = License::all();
        return view('admin.print.licenses', compact('licenses'));
    }

    private function adminPrintPermits()
    {
        $permits = Permit::all();
        return view('admin.print.permits', compact('permits'));
    }

    private function adminPrintIndustries()
    {
        $industries = Industry::all();
        return view('admin.print.industries', compact('industries'));
    }

    private function adminPrintRegistered()
    {
        $businesses = Business::all();
        return view('admin.print.registered', compact('businesses'));
    }

    private function adminPrintLicenseSubs()
    {
        $subscriptions = LicenseSubscription::all();
        return view('admin.print.license_subs', compact('subscriptions'));
    }

    private function adminPrintPermitSubs()
    {
        $subscriptions = PermitSubscription::all();
        return view('admin.print.permit_subs', compact('subscriptions'));
    }
}
