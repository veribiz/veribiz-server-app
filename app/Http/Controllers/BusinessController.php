<?php

namespace App\Http\Controllers;

use App\Business;
use App\BusinessRegistrationApplication;
use App\Industry;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Auth;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class BusinessController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin')->only('admin');
        $this->middleware('auth')->only('index');
    }

    public function index()
    {
        switch (request('q')) {
            case 'view':
                return $this->viewBusinesses();
            case 'apply':
                return $this->apply();
            case 'pending':
                return $this->viewPending();
            case 'approved':
                return $this->viewApproved();
            case 'rejected':
                return $this->viewRejected();
            case 'cancelled':
                return $this->viewCancelled();
            case 'cancel':
                return $this->cancelApplication();
            case 'print':
                return $this->generateBusinessCode();
            default:
                return back();
        }
    }

    public function admin()
    {
        switch (request('q')) {
            case 'view':
                return $this->adminViewBusinesses();
            case 'pending':
                return $this->adminViewPending();
            case 'approved':
                return $this->adminViewApproved();
            case 'rejected':
                return $this->adminViewRejected();
            case 'cancelled':
                return $this->adminViewCancelled();
            case 'approve':
                return $this->approveApplication();
            case 'reject':
                return $this->rejectApplication();
            default:
                return back();
        }
    }

    //simple user functions
    private function viewBusinesses()
    {
        $businesses = Business::where('user_id', Auth::user()->id)->paginate(9);
        return view('app.business.registered', compact('businesses'));
    }

    private function apply()
    {
        if ($this->get()) {
            return view('app.business.application',
                ['industries' => Industry::all()]
            );
        }
        if ($this->post()) {
            BusinessRegistrationApplication::create(
                [
                    'name' => request('name'),
                    'phone_a' => request('phone_a'),
                    'phone_b' => request('phone_b'),
                    'email' => request('email'),
                    'location' => request('location'),
                    'lat' => request('latitude'),
                    'lng' => request('longitude'),
                    'description' => request('description'),
                    'user_id' => Auth::user()->id,
                    'industry_id' => request('industry')
                ]
            );
            $msg = "Made application for business registration.";
            return view('app.business.application',
                ["msg" => $msg, 'industries' => Industry::all()]
            );
        }
    }

    private function viewPending()
    {
        if ($this->get()) {
            $pendingApps = BusinessRegistrationApplication::where(
                [
                    'status' => 0,
                    'user_id' => Auth::user()->id
                ]
            )->paginate(9);
            return view('app.business.pending', ["applications" => $pendingApps]);
        }
    }

    private function viewApproved()
    {
        if ($this->get()) {
            $approvedApps = BusinessRegistrationApplication::where(
                [
                    'status' => 1,
                    'user_id' => Auth::user()->id
                ]
            )->paginate(9);
            return view('app.business.approved', ["applications" => $approvedApps]);
        }
    }

    private function viewRejected()
    {
        if ($this->get()) {
            $rejectedApps = BusinessRegistrationApplication::where(
                [
                    'status' => 2,
                    'user_id' => Auth::user()->id
                ]
            )->paginate(9);
            return view('app.business.rejected', ["applications" => $rejectedApps]);
        }
    }

    private function viewCancelled()
    {
        if ($this->get()) {
            $cancelledApps = BusinessRegistrationApplication::where(
                [
                    'status' => 3,
                    'user_id' => Auth::user()->id
                ]
            )->paginate(9);
            return view('app.business.cancelled', ["applications" => $cancelledApps]);
        }
    }

    private function cancelApplication()
    {
        if ($this->get()){
            $appId = request('id');
            $application = BusinessRegistrationApplication::find($appId);
            //check if app belongs to user first
            if($application->user_id == Auth::user()->id)
            {
                //mark application as cancelled
                $application->status = 3;
                $application->action_by = Auth::user()->id;
                $application->save();
                return back();
            }
        }
    }

    private function generateBusinessCode()
    {
        $biz = request('id');

        $data = QrCode::format('png')->size(350)->margin(1)->generate($biz);

        $headers = array(
            'Content-Type' => 'image/png',
            'Content-Disposition' => 'inline; filename="' . $biz . '.png"'
        );

        return response($data, 200, $headers);
    }

    //admin functions
    private function adminViewBusinesses()
    {
        if ($this->get()) {
            $businesses = Business::paginate(9);
            return view('admin.business.registered', compact('businesses'));
        }
    }

    private function adminViewPending()
    {
        $applications = BusinessRegistrationApplication::where(['status' => 0])
            ->orderBy('updated_at', 'desc')->paginate(9);
        return view('admin.business.pending', compact('applications'));
    }

    private function adminViewApproved()
    {
        $applications = BusinessRegistrationApplication::where(['status' => 1])->paginate(9);
        return view('admin.business.approved', compact('applications'));
    }

    private function adminViewRejected()
    {
        $applications = BusinessRegistrationApplication::where(['status' => 2])
            ->orderBy('updated_at', 'desc')->paginate(9);
        return view('admin.business.rejected', compact('applications'));
    }

    private function adminViewCancelled()
    {
        $applications = BusinessRegistrationApplication::where(['status' => 3])
            ->orderBy('updated_at', 'desc')->paginate(9);
        return view('admin.business.cancelled', compact('applications'));
    }

    private function approveApplication()
    {
        if ($this->get()){
            $appId = request('id');
            $application = BusinessRegistrationApplication::find($appId);
            //create business
            Business::create(
                [
                    'name' => $application->name,
                    'phone_a' => $application->phone_a,
                    'phone_b' => $application->phone_b,
                    'email' => $application->email,
                    'location' => $application->location,
                    'lat' => $application->lat,
                    'lng' => $application->lng,
                    'description' => $application->description,
                    'user_id' => $application->user_id,
                    'industry_id' => $application->industry_id
                ]
            );
            //mark application as approved
            $application->status = 1;
            $application->action_by = Auth::user()->id;
            $application->save();
            return back();
        }
    }

    private function rejectApplication()
    {
        if ($this->get()){
            $appId = request('id');
            $application = BusinessRegistrationApplication::find($appId);
            //mark application as rejected
            $application->status = 2;
            $application->action_by = Auth::user()->id;
            $application->save();
            return back();
        }
    }

    //api
    public function apiAll()
    {
        $business = request('biz');
        $details = Business::where('id', $business)->first();
        $bizFilter = ['business_id' => $business];
        if ($details)
        {
            $details->registered = true;
            if (request('gps'))
            {
                $latitude = request('lat');
                $longitude = request('lng');
                $distance = self::vincentyGreatCircleDistance(
                    $details->lat, $details->lng, $latitude, $longitude
                );
                $distance = sprintf('%0.2f', $distance);
                $details->distance = $distance;
                $details->distanceThresholdExceeded = ($distance > 50);
            }
            $details->licenseSubs = $this->getLicenseSubs($bizFilter);
            $details->permitSubs = $this->getPermitSubs($bizFilter);
            $details->licenseApps = $this->getLicenseApps($bizFilter);
            $details->permitApps = $this->getPermitApps($bizFilter);
            return Response::json($details, 200, [], JSON_PRETTY_PRINT);
        }
        else
        {
            return json_encode(['registered' => false]);
        }
    }

    /**
     * Calculates the great-circle distance between two points, with
     * the Vincenty formula.
     * @param float $latitudeFrom Latitude of start point in [deg decimal]
     * @param float $longitudeFrom Longitude of start point in [deg decimal]
     * @param float $latitudeTo Latitude of target point in [deg decimal]
     * @param float $longitudeTo Longitude of target point in [deg decimal]
     * @param float $earthRadius Mean earth radius in [m]
     * @return float Distance between points in [m] (same as earthRadius)
     */
    public static function vincentyGreatCircleDistance(
        $latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371000)
    {
        // convert from degrees to radians
        $latFrom = deg2rad($latitudeFrom);
        $lonFrom = deg2rad($longitudeFrom);
        $latTo = deg2rad($latitudeTo);
        $lonTo = deg2rad($longitudeTo);

        $lonDelta = $lonTo - $lonFrom;
        $a = pow(cos($latTo) * sin($lonDelta), 2) +
            pow(cos($latFrom) * sin($latTo) - sin($latFrom) * cos($latTo) * cos($lonDelta), 2);
        $b = sin($latFrom) * sin($latTo) + cos($latFrom) * cos($latTo) * cos($lonDelta);

        $angle = atan2(sqrt($a), $b);
        return $angle * $earthRadius;
    }

    public function getLicenseSubs($bizFilter)
    {
        return DB::table('license_subscriptions')
            ->join('licenses', function($join){
                $join->on('license_subscriptions.license_id', '=', 'licenses.id');
            })
            ->where($bizFilter)
            ->where("expiry", ">", Carbon::now())//discard those already expired
            ->select(DB::raw(
                'licenses.name as name, 
                    license_subscriptions.fee as fee, 
                    license_subscriptions.commencement as commencement,
                    license_subscriptions.expiry as expiry'
            ))
            ->get();
    }

    public function getPermitSubs($bizFilter)
    {
        return DB::table('permit_subscriptions')
            ->join('permits', function($join){
                $join->on('permit_subscriptions.permit_id', '=', 'permits.id');
            })
            ->where($bizFilter)
            ->where("expiry", ">", Carbon::now())//discard those already expired
            ->select(DB::raw(
                'permits.name as name, 
                    permit_subscriptions.fee as fee, 
                    permit_subscriptions.commencement as commencement,
                    permit_subscriptions.expiry as expiry'
            ))
            ->get();
    }

    public function getLicenseApps($bizFilter)
    {
        $bizFilter["status"] = 1;//discard those already approved
        $bizFilter["license_applications.deleted_at"] = null;//discard those already deleted
        return DB::table('license_applications')
            ->join('licenses', function($join){
                $join->on('license_applications.license_id', '=', 'licenses.id');
            })
            ->where($bizFilter)
            ->select(DB::raw(
                'licenses.name as name, 
                    license_applications.duration as duration, 
                    license_applications.created_at as date'
            ))
            ->get();
    }

    public function getPermitApps($bizFilter)
    {
        $bizFilter["status"] = 1;//discard those already approved
        $bizFilter["permit_applications.deleted_at"] = null;//discard those already deleted
        return DB::table('permit_applications')
            ->join('permits', function($join){
                $join->on('permit_applications.permit_id', '=', 'permits.id');
            })
            ->where($bizFilter)
            ->select(DB::raw(
                'permits.name as name, 
                    permit_applications.duration as duration, 
                    permit_applications.created_at as date'
            ))
            ->get();
    }
}
