<?php

namespace App\Http\Controllers;

use App\Business;
use App\BusinessRegistrationApplication;
use App\Industry;

class IndustryController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin')->only('admin');
        $this->middleware('auth')->only('index');
    }

    public function index()
    {
        switch (request('q')) {
            case 'industries':
                return $this->viewIndustries();
            default:
                return back();
        }
    }

    public function admin()
    {
        switch (request('q')) {
            case 'add':
                return $this->addIndustry();
            case 'industries':
                return $this->adminViewIndustries();
            case 'delete':
                return $this->delete();
            default:
                return back();
        }
    }

    //simple user functions
    private function viewIndustries()
    {
        if ($this->get()) {
            $industries = Industry::paginate(9);
            return view('app.industry.industries', compact('industries'));
        }
    }

    //admin functions
    private function addIndustry()
    {
        if ($this->get())
        {
            return view('admin.industry.add');
        }
        if ($this->post())
        {
            Industry::create(
                [
                    "name" => request('name'),
                    "description" => request('description')
                ]
            );
            $msg = "Added industry.";
            return view('admin.industry.add', ["msg" => $msg]);
        }
    }

    private function adminViewIndustries()
    {
        if ($this->get()) {
            $industries = Industry::paginate(9);
            return view('admin.industry.industries', compact('industries'));
        }
        if ($this->post())
        {
            $industry = Industry::find(request('industry'));
            $industry->name = request('name');
            $industry->description = request('description');
            $industry->save();
            $msg = "Edited industry";
            return view('admin.industry.industries',
                ["msg" => $msg, 'industries' => Industry::paginate(9)]
            );
        }
    }

    private function delete()
    {
        if ($this->get()){
            $industryId = request('id');
            //check if there are subscriptions, applications
            $subscriptions = Business::where('industry_id', $industryId)->first();
            $applications = BusinessRegistrationApplication::where('industry_id', $industryId)->first();
            if (count($subscriptions) > 0 || count($applications))
            {
                //there are subscriptions. Abort
                $fail = "The permit cannot be deleted as there are businesses or applications that rely on it";
                return redirect()->back()->with('fail', $fail);
            }
            else
            {
                //no subscriptions
                Industry::find($industryId)->delete();
                $msg = "Deleted permit";
                return redirect()->back()->with('msg', $msg);
            }
        }
        return redirect()->back();
    }
}
