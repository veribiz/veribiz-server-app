<?php

namespace App\Http\Controllers;
use App\Business;
use App\License;
use App\LicenseApplication;
use App\LicenseSubscription;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class LicenseController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin')->only('admin');
        $this->middleware('auth')->only('index');
    }

    public function index()
    {
        switch (request('q')) {
            case 'subscriptions':
                return $this->viewSubscriptions();
            case 'licenses':
                return $this->viewLicenses();
            case 'apply':
                return $this->apply();
            case 'pending':
                return $this->viewPending();
            case 'approved':
                return $this->viewApproved();
            case 'rejected':
                return $this->viewRejected();
            case 'cancelled':
                return $this->viewCancelled();
            case 'cancel':
                return $this->cancelApplication();
            default:
                return back();
        }
    }

    public function admin()
    {
        switch (request('q')) {
            case 'add':
                return $this->addLicense();
            case 'licenses':
                return $this->adminViewLicenses();
            case 'subscriptions':
                return $this->adminViewSubscriptions();
            case 'pending':
                return $this->adminViewPending();
            case 'approved':
                return $this->adminViewApproved();
            case 'rejected':
                return $this->adminViewRejected();
            case 'cancelled':
                return $this->adminViewCancelled();
            case 'approve':
                return $this->approveApplication();
            case 'reject':
                return $this->rejectApplication();
            case 'delete':
                return $this->delete();
            default:
                return back();
        }
    }

    //simple user functions
    private function viewSubscriptions()
    {
        if(empty(request('id')))
        {
            if(!$this->hasBusinesses())
            {
                //no businesses
                return view('app.license.subscriptions', ["business" => null]);
            }
            else
            {
                //get first business
                $business_id = $this->getBusinesses()[0]->id;
            }
        }
        else
        {
            $business_id = Business::find(request('id'))->id;
        }
        //get business licenses
        $subscriptions = $this->getLicensesSubscriptions($business_id);
        return view('app.license.subscriptions',
            [
                "business" => $business_id,
                "businesses" => $this->getBusinesses(),
                "subscriptions" => $subscriptions
            ]
        );
    }

    private function viewLicenses()
    {
        if ($this->get()) {
            $licenses = License::paginate(9);
            return view('app.license.licenses', compact('licenses'));
        }
    }

    private function apply()
    {
        if ($this->get()) {
            return view('app.license.application',
                [
                    "businesses" => $this->getBusinesses(),
                    "licenses" => $this->getLicenses()
                ]
            );
        }
        if ($this->post()) {
            LicenseApplication::create(
                [
                    'business_id' => request('business'),
                    'license_id' => request('license'),
                    'duration' => request('duration')
                ]
            );
            $msg = "Made application for business license.";
            return view('app.license.application',
                [
                    "msg" => $msg,
                    "businesses" => $this->getBusinesses(),
                    "licenses" => $this->getLicenses()
                ]
            );
        }
    }

    private function viewPending()
    {
        if ($this->get()) {
            $pendingApps = LicenseApplication::whereHas('business', function ($query){
                $query->where('user_id', Auth::user()->id);
            })
            ->where(['status' => 0])->paginate(9);
            return view('app.license.pending', ['applications' => $pendingApps]);
        }
    }

    private function viewApproved()
    {
        if ($this->get()) {
            $approvedApps = LicenseApplication::whereHas('business', function ($query){
                $query->where('user_id', Auth::user()->id);
            })
                ->where(['status' => 1])->paginate(9);
            return view('app.license.approved', ['applications' => $approvedApps]);
        }
    }

    private function viewRejected()
    {
        if ($this->get()) {
            $rejectedApps = LicenseApplication::whereHas('business', function ($query){
                $query->where('user_id', Auth::user()->id);
            })
                ->where(['status' => 2])->paginate(9);
            return view('app.license.rejected', ['applications' => $rejectedApps]);
        }
    }

    private function viewCancelled()
    {
        if ($this->get()) {
            $cancelledApps = LicenseApplication::whereHas('business', function ($query){
                $query->where('user_id', Auth::user()->id);
            })
                ->where(['status' => 3])->paginate(9);
            return view('app.license.cancelled', ['applications' => $cancelledApps]);
        }
    }

    private function cancelApplication()
    {
        if ($this->get()){
            $appId = request('id');
            $application = LicenseApplication::find($appId);
            //check if app belongs to user first
            if($application->business->user_id == Auth::user()->id)
            {
                //mark application as cancelled
                $application->status = 3;
                $application->action_by = Auth::user()->id;
                $application->save();
                $msg = "Cancelled license application";
                return redirect()->back()->with('msg', $msg);
            }
        }
        return redirect()->back();
    }

    //admin functions
    private function addLicense()
    {
        if ($this->get())
        {
            return view('admin.license.add');
        }
        if ($this->post())
        {
            License::create(
                [
                    "name" => request('name'),
                    "fee" => request('fee'),
                    "description" => request('description')
                ]
            );
            $msg = "Added license.";
            return view('admin.license.add', ["msg" => $msg]);
        }
    }

    private function adminViewLicenses()
    {
        if ($this->get()) {
            $licenses = License::paginate(9);
            return view('admin.license.licenses', compact('licenses'));
        }
        if ($this->post())
        {
            $license = License::find(request('license'));
            $license->name = request('name');
            $license->fee = request('fee');
            $license->description = request('description');
            $license->save();
            $msg = "Edited license";
            return view('admin.license.licenses',
                ["msg" => $msg, 'licenses' => License::paginate(9)]
            );
        }
    }

    private function adminViewSubscriptions()
    {
        $subscriptions = LicenseSubscription::paginate(9);
        return view('admin.license.subscriptions', ['subscriptions' => $subscriptions]);
    }

    private function adminViewPending()
    {
        $applications = LicenseApplication::where(['status' => 0])->paginate(9);
        return view('admin.license.pending',
            ['applications' => $applications]
        );
    }

    private function adminViewApproved()
    {
        $applications = LicenseApplication::where(['status' => 1])->paginate(9);
        return view('admin.license.approved',
            ['applications' => $applications]
        );
    }

    private function adminViewRejected()
    {
        $applications = LicenseApplication::where(['status' => 2])->paginate(9);
        return view('admin.license.rejected',
            ['applications' => $applications]
        );
    }

    private function adminViewCancelled()
    {
        $applications = LicenseApplication::where(['status' => 3])->paginate(9);
        return view('admin.license.cancelled',
            ['applications' => $applications]
        );
    }

    private function approveApplication()
    {
        if ($this->get())
        {
            $appId = request('id');
            $application = LicenseApplication::find($appId);
            //create license subscription
            /*
             * issues to consider:
             * -duration: how to apply it to subscription
             * -duplicate subscription
             * commencement: now
             * expiry: now + duration
            */
            $date = Carbon::now();
            $commencement = $date->toDateTimeString();
            $expiry = $date->addMonths($application->duration)->toDateTimeString();
            LicenseSubscription::create(
                [
                    'business_id' => $application->business_id,
                    'license_id' => $application->license_id,
                    'fee' => $application->license->fee,
                    'commencement' => $commencement,
                    'expiry' => $expiry
                ]
            );
            //mark application as approved
            $application->status = 1;
            $application->action_by = Auth::user()->id;
            $application->save();
            $msg = "Approved license application";
            return redirect()->back()->with('msg', $msg);
        }
        return redirect()->back();
    }

    private function rejectApplication()
    {
        if ($this->get()){
            $appId = request('id');
            $application = LicenseApplication::find($appId);
            //mark application as rejected
            $application->status = 2;
            $application->action_by = Auth::user()->id;
            $application->save();
            $msg = "Rejected license application";
            return redirect()->back()->with('msg', $msg);
        }
        return redirect()->back();
    }

    private function delete()
    {
        if ($this->get()){
            $licenseId = request('id');
            //check if there are subscriptions
            $subscriptions = LicenseSubscription::where('license_id', $licenseId)->first();
            $applications = LicenseApplication::where('license_id', $licenseId)->first();
            if (count($subscriptions) > 0 || count($applications))
            {
                //there are subscriptions. Abort
                $fail = "The license cannot be deleted as there are subscriptions or applications that rely on it";
                return redirect()->back()->with('fail', $fail);
            }
            else
            {
                //no subscriptions
                License::find($licenseId)->delete();
                $msg = "Deleted license";
                return redirect()->back()->with('msg', $msg);
            }
        }
        return redirect()->back();
    }

    //utility functions
    private function hasBusinesses(){
        $businesses = Business::where('user_id', Auth::user()->id)->get();
        if (count($businesses)) {
            return true;
        }
        else {
            return false;
        }
    }

    private function getBusinesses(){
        return Business::where('user_id', Auth::user()->id)->get();
    }

    private function getLicensesSubscriptions($business){
        return LicenseSubscription::where('business_id', $business)->paginate(8);
    }

    private function getLicenses(){
        return License::all();
    }
}
