@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-md-offset-0">
                <div class="panel panel-default">
                    <div class="panel-heading">Industries</div>
                    <div class="panel-body">
                        @if(count($industries) > 0)
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Description</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($industries as $industry)
                                    <tr>
                                        <td> {{ $industry->name }} </td>
                                        <td> {{ $industry->description}} </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {!! $industries->appends(['q' => 'industries'])->render() !!}
                        @else
                            <div class="alert alert-info">
                                There are no industries!
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection