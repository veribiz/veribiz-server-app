@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-md-offset-0">
                <div class="panel panel-default">
                    <div class="panel-heading">Permits</div>
                    <div class="panel-body">
                        @if(count($permits) > 0)
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Fee</th>
                                    <th>Description</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($permits as $permit)
                                    <tr>
                                        <td> {{ $permit->name }} </td>
                                        <td> {{ $permit->fee }} </td>
                                        <td> {{ $permit->description}} </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {!! $permits->appends(['q' => 'permits'])->render() !!}
                        @else
                            <div class="alert alert-info">
                                There are no permits!
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection