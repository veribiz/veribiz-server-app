@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Apply for business permit</div>
                    <div class="panel-body">
                        @if ( !empty ( $msg ) )
                            <div class="alert alert-success alert-dismissable">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong>Success!</strong> {{$msg}}
                            </div>
                        @endif
                        @if(count($businesses) && count($permits))
                            <div class="col-sm-7 col-sm-offset-2">
                                <form method="post" class="form-horizontal">
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Business</label>
                                        <div class="col-sm-10">
                                            <select class="form-control" name="business" required>
                                                @foreach($businesses as $business)
                                                    <option value="{{$business->id}}">{{$business->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Permit</label>
                                        <div class="col-sm-10">
                                            <select class="form-control" name="permit" required>
                                                @foreach($permits as $permit)
                                                    <option value="{{$permit->id}}">{{$permit->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Duration months</label>
                                        <div class="col-sm-10">
                                            <input class="form-control" name="duration" min="0" type="number">
                                        </div>
                                    </div>
                                    {{csrf_field()}}
                                    <div class="form-group">
                                        <div class="col-sm-10 col-sm-offset-10">
                                            <button type="submit" class="btn btn-default">Apply</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        @else
                            @if(!count($businesses))
                                <div class="alert alert-info">
                                    You don't have any registered businesses yet!
                                </div>
                            @endif
                            @if(!count($permits))
                                <div class="alert alert-info">
                                    There are no permits available for application!
                                </div>
                            @endif
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection