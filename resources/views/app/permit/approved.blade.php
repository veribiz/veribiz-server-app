@extends('layouts.app')

@section('content')
    @include('includes.confirm_op')
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-md-offset-0">
                <div class="panel panel-default">
                    <div class="panel-heading">My approved business permit applications</div>
                    <div class="panel-body">
                        @if ( Session::has('msg') )
                            <div class="alert alert-success alert-dismissable">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong>Success!</strong> {{Session::get("msg")}}
                            </div>
                        @endif
                        @if(count($applications))
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Business</th>
                                    <th>Permit</th>
                                    <th>Duration</th>
                                    <th>Approved on</th>
                                </tr>
                                </thead>
                                @foreach($applications as $application)
                                    <tr>
                                        <td> {{ $application->business->name }} </td>
                                        <td> {{ $application->permit->name }} </td>
                                        <td> {{ $application->duration }} months</td>
                                        <td> {{ $application->updated_at }}</td>
                                    </tr>
                                @endforeach
                            </table>
                            {!! $applications->appends(['q' => 'approved'])->render() !!}
                        @else
                            <div class="alert alert-info">
                                You don't have approved business permit applications!
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection