@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-md-offset-0">
                <div class="panel panel-default">
                    <div class="panel-heading">My permits</div>
                    <div class="panel-body">
                        @if(!empty($business))
                            <input id="initial_biz" value="{{$business}}" hidden>
                            <form method="post" id="bus_sel_form">
                                <div class="col-md-4">
                                    {{csrf_field()}}
                                    <select class="form-control" id="bus_select" name="id">
                                        @foreach($businesses as $bus)
                                            <option value="{{$bus->id}}">{{$bus->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </form>
                            <br><br>
                            @if(!empty($subscriptions) && count($subscriptions) > 0)
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Permit</th>
                                        <th>Fee</th>
                                        <th>Commencement</th>
                                        <th>Expiry</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($subscriptions as $subscription)
                                        <tr>
                                            <td> {{ $subscription->permit->name }} </td>
                                            <td> {{ $subscription->fee }} </td>
                                            <td> {{ $subscription->commencement }} </td>
                                            <td> {{ $subscription->expiry }} </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                {!! $subscriptions->appends(['q' => 'subscriptions'])->render() !!}
                            @else
                                <div class="alert alert-info">
                                    There are no permits for this business!
                                </div>
                            @endif
                        @else
                            <div class="alert alert-info">You don't have any registered businesses yet!</div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection