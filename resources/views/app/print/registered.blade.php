<div class="panel-heading">Registered businesses</div>
<div class="panel-body">
    @if(count($businesses) > 0)
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>Name</th>
                <th>Phone a</th>
                <th>Phone b</th>
                <th>Email</th>
                <th>Location</th>
                <th>Industry</th>
                <th>Registered on</th>
            </tr>
            </thead>
            <tbody>
            @foreach($businesses as $business)
                <tr>
                    <td> {{ $business->name }} </td>
                    <td> {{ $business->phone_a }} </td>
                    <td> {{ $business->phone_b }} </td>
                    <td> {{ $business->email }} </td>
                    <td> {{ $business->location }} </td>
                    <td> {{ $business->industry->name }} </td>
                    <td> {{ $business->created_at }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @else
        <div>
            You have no registered businesses!
        </div>
    @endif
</div>