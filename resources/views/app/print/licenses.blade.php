<div class="panel-heading">Licenses</div>
<div class="panel-body">
    @if(count($licenses) > 0)
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>Name</th>
                <th>Fee</th>
                <th>Description</th>
            </tr>
            </thead>
            <tbody>
            @foreach($licenses as $license)
                <tr>
                    <td> {{ $license->name }} </td>
                    <td> {{ $license->fee }} </td>
                    <td> {{ $license->description}} </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @else
        <div>
            There are no licenses!
        </div>
    @endif
</div>

