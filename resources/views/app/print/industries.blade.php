<div class="panel-heading">Industries</div>
<div class="panel-body">
    @if(count($industries) > 0)
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>Name</th>
                <th>Description</th>
            </tr>
            </thead>
            <tbody>
            @foreach($industries as $industry)
                <tr>
                    <td> {{ $industry->name }} </td>
                    <td> {{ $industry->description}} </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @else
        <div>
            There are no industries!
        </div>
    @endif
</div>

