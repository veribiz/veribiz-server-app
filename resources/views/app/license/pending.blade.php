@extends('layouts.app')

@section('content')
    @include('includes.confirm_op')
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-md-offset-0">
                <div class="panel panel-default">
                    <div class="panel-heading">My pending business license applications</div>
                    <div class="panel-body">
                        @if ( Session::has('msg') )
                            <div class="alert alert-success alert-dismissable">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong>Success!</strong> {{Session::get("msg")}}
                            </div>
                        @endif
                        @if(count($applications))
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Business</th>
                                    <th>License</th>
                                    <th>Duration</th>
                                    <th>Applied on</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                @foreach($applications as $application)
                                    <tr>
                                        <td> {{ $application->business->name }} </td>
                                        <td> {{ $application->license->name }} </td>
                                        <td> {{ $application->duration }} months</td>
                                        <td> {{ $application->created_at }}</td>
                                        <td>
                                            <a class="btn btn-default btn-sm"
                                               href="{{url()->current() . '?q=cancel&id=' . $application->id}}"
                                               id="_cancel_action_btn">
                                                Cancel
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                            {!! $applications->appends(['q' => 'pending'])->render() !!}
                        @else
                            <div class="alert alert-info">
                                You don't have pending business license applications!
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
