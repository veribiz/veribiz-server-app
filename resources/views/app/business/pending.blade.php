@extends('layouts.app')

@section('content')
    @include('includes.app_bus_detailed')
    @include('includes.confirm_op')
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-md-offset-0">
                <div class="panel panel-default">
                    <div class="panel-heading">My pending business registration applications</div>
                    <div class="panel-body">
                        @if(count($applications))
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Business name</th>
                                    <th>Phone b</th>
                                    <th>Email</th>
                                    <th>Location</th>
                                    <th>Industry</th>
                                    <th>Applied on</th>
                                    <th>View</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                @foreach($applications as $application)
                                    <tr>
                                        <td> {{ $application->name }} </td>
                                        <td> {{ $application->phone_a }} </td>
                                        <td> {{ $application->email }}</td>
                                        <td> {{ $application->location }}</td>
                                        <td> {{ $application->industry->name }} </td>
                                        <td> {{ $application->created_at }}</td>
                                        <td>
                                            <button class="btn btn-default btn-sm"
                                                    id="app_biz_view_detailed"
                                                    value="{{json_encode(
                                                    [
                                                        'name' => $application->name,
                                                        'phone_a' => $application->phone_a,
                                                        'phone_b' => $application->phone_b,
                                                        'email' => $application->email,
                                                        'location' => $application->location,
                                                        'coordinates_d' => $application->lat . ', ' . $application->lng,
                                                        'coordinates' => $application->lat . ', ' . $application->lng,
                                                        'description' => $application->description,
                                                        'industry' => $application->industry->name,
                                                        'date' =>  $application->updated_at->format('d-m-Y H:i:s'),
                                                        'date_title' => 'Applied on'
                                                    ])}}">
                                                Detailed
                                            </button>
                                        </td>
                                        <td>
                                            <a class="btn btn-default btn-sm"
                                               href="{{url()->current() . '?q=cancel&id=' . $application->id}}"
                                               id="_cancel_action_btn">
                                                Cancel
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                            {!! $applications->appends(['q' => 'pending'])->render() !!}
                        @else
                            <div class="alert alert-info">
                                You don't have pending business registration applications!
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
