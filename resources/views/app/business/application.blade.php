@extends('layouts.app')

@section('content')
    @include('includes.info')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Apply for business registration</div>
                    <div class="panel-body">
                        @if ( !empty ( $msg ) )
                            <div class="alert alert-success alert-dismissable">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong>Success!</strong> {{$msg}}
                            </div>
                        @endif
                        <div class="col-sm-8 col-sm-offset-2">
                            <form method="post" class="form-horizontal">
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Name</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" name="name" type="text" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Phone a</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" name="phone_a" type="text" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Phone b</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" name="phone_b" type="text" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Email</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" name="email" type="email" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Location</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" name="location" type="text" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Coordinates</label>
                                    <div class="col-sm-4">
                                        <input class="form-control" name="latitude" id="lat" type="text" readonly required>
                                    </div>
                                    <div class="col-sm-4">
                                        <input class="form-control" name="longitude" id="long" type="text" readonly required>
                                    </div>
                                    <div class="col-sm-2">
                                        <button class="btn btn-default" id="detect_gps" type="button">Detect</button>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Description</label>
                                    <div class="col-sm-10">
                                        <textarea class="form-control" name="description" type="text"
                                                  required></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Industry</label>
                                    <div class="col-sm-10">
                                        <select class="form-control" name="industry">
                                            @foreach($industries as $industry)
                                            <option value="{{$industry->id}}">{{$industry->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                {{csrf_field()}}
                                <div class="form-group">
                                    <div class="col-sm-offset-9">
                                        <button type="reset" class="btn btn-default">Cancel</button>
                                        <button type="submit" class="btn btn-default">Apply</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
