@extends('layouts.app')

@section('content')
    @include('includes.printout')
    @include('includes.app_bus_detailed')
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-md-offset-0">
                <div class="panel panel-default">
                    <div class="panel-heading">My registered businesses</div>
                    <div class="panel-body">
                        <table class="table table-bordered">
                            @if(count($businesses) > 0)
                                <thead>
                                <tr>
                                    <th>Business name</th>
                                    <th>Phone a</th>
                                    <th>Email</th>
                                    <th>Location</th>
                                    <th>Industry</th>
                                    <th>Registered on</th>
                                    <th colspan="2">View</th>
                                </tr>
                                </thead>
                                @foreach($businesses as $business)
                                    <tr>
                                        <td> {{ $business->name }} </td>
                                        <td> {{ $business->phone_a }} </td>
                                        <td> {{ $business->email }}</td>
                                        <td> {{ $business->location }}</td>
                                        <td> {{ $business->industry->name}}</td>
                                        <td> {{ $business->created_at}}</td>
                                        <td>
                                            <button class="btn btn-default btn-sm"
                                                    id="app_biz_view_detailed"
                                                    value="{{json_encode(
                                                        [
                                                            'name' => $business->name,
                                                            'phone_a' => $business->phone_a,
                                                            'phone_b' => $business->phone_b,
                                                            'email' => $business->email,
                                                            'location' => $business->location,
                                                            'coordinates_d' => $business->lat . ', ' . $business->lng,
                                                            'coordinates' => $business->lat . ',' . $business->lng,
                                                            'description' => $business->description,
                                                            'owner' => $business->user->name,
                                                            'industry' => $business->industry->name,
                                                            'date' =>  $business->created_at->format('d-m-Y H:i:s'),
                                                            'date_title' => 'Registered on'
                                                        ])}}">
                                                Detailed
                                            </button>
                                        </td>
                                        <td>
                                            <a class="btn btn-default btn-sm"
                                               href="{{url()->current() . '?q=print&id=' . $business->id}}"
                                               id="_show_printout">
                                                Reg cert
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </table>
                                {!! $businesses->appends(['q' => 'view'])->render() !!}
                            @else
                                <div class="alert alert-info">
                                    You don't have any registered business!
                                </div>
                            @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
