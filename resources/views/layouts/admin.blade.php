<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<body>
<div id="app">
    <input type="hidden" value="{{url('/admin/')}}" id="base_url">
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            Businesses
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="{{ url('admin/business?q=view') }}">Registered businesses</a></li>
                            <li><a href="{{ url('admin/business?q=pending') }}">Pending applications</a></li>
                            <li><a href="{{ url('admin/business?q=approved') }}">Approved applications</a></li>
                            <li><a href="{{ url('admin/business?q=rejected') }}">Rejected applications</a></li>
                            <li><a href="{{ url('admin/business?q=cancelled') }}">Cancelled applications</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            Licenses
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="{{ url('admin/license?q=add') }}">Add license</a></li>
                            <li><a href="{{ url('admin/license?q=licenses') }}">Licenses</a></li>
                            <li><a href="{{ url('admin/license?q=subscriptions') }}">License subscriptions</a></li>
                            <li><a href="{{ url('admin/license?q=pending') }}">Pending applications</a></li>
                            <li><a href="{{ url('admin/license?q=approved') }}">Approved applications</a></li>
                            <li><a href="{{ url('admin/license?q=rejected') }}">Rejected applications</a></li>
                            <li><a href="{{ url('admin/license?q=cancelled') }}">Cancelled applications</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            Permits
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="{{ url('admin/permit?q=add') }}">Add permit</a></li>
                            <li><a href="{{ url('admin/permit?q=permits') }}">Permits</a></li>
                            <li><a href="{{ url('admin/permit?q=subscriptions') }}">Permit subscriptions</a></li>
                            <li><a href="{{ url('admin/permit?q=pending') }}">Pending applications</a></li>
                            <li><a href="{{ url('admin/permit?q=approved') }}">Approved applications</a></li>
                            <li><a href="{{ url('admin/permit?q=rejected') }}">Rejected applications</a></li>
                            <li><a href="{{ url('admin/permit?q=cancelled') }}">Cancelled applications</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            Industries
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="{{ url('admin/industry?q=add') }}">Add industries</a></li>
                            <li><a href="{{ url('admin/industry?q=industries') }}">Industries</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            Print
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a id="_print" href="#">Licenses</a></li>
                            <li><a id="_print" href="#">Permits</a></li>
                            <li><a id="_print" href="#">Industries</a></li>
                            <li><a id="_print" href="#">Registered businesses</a></li>
                            <li><a id="_print" href="#">License subscriptions</a></li>
                            <li><a id="_print" href="#">Permit subscriptions</a></li>
                        </ul>
                    </li>
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li><a href="{{ route('login') }}">Login</a></li>
                        <li><a href="{{ route('register') }}">Register</a></li>
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li>
                                    <a href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>
    @yield('content')
</div>

<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ url('js/printThis.js') }}"></script>
<script src="{{ url('js/veribiz.js') }}"></script>
</body>
</html>
