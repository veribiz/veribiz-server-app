<div id="app_bus_detailed_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    ×
                </button>
                <h3 id="app_biz_detail_name"></h3>
            </div>
            <div class="modal-body">
                <table class="table table-bordered">
                    <tbody>
                    <tr>
                        <th>Phone a</th>
                        <td id="phone_a"></td>
                    </tr>
                    <tr>
                        <th>Phone b</th>
                        <td id="phone_b"></td>
                    </tr>
                    <tr>
                        <th>Email</th>
                        <td id="email"></td>
                    </tr>
                    <tr>
                        <th>Location</th>
                        <td id="location"></td>
                    </tr>
                    <tr>
                        <th>Coordinates</th>
                        <td>
                            <span id="coordinates"></span>&nbsp;
                            <a id="go_to_map" class="btn btn-default btn-sm" target="_blank">
                                Go to map
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <th>Description</th>
                        <td id="description"></td>
                    </tr>
                    <tr>
                        <th>Industry</th>
                        <td id="industry"></td>
                    </tr>
                    <tr>
                        <th id="date_title"></th>
                        <td id="date"></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true">Close</button>
            </div>
        </div>
    </div>
</div>