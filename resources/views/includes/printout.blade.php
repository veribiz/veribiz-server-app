<div id="printout_modal" class="modal fade" role="dialog">
    <div id="_printout" class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    ×
                </button>
                <h3>Registration certificate</h3>
            </div>
            <div class="modal-body">
                <div id="print_area">
                    <br>
                    <b id="bus_name"></b>
                    <div id="qr_code"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button id="print_reg_cert" class="btn btn-primary">Print cert</button>
                <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true">Cancel</button>
            </div>
        </div>
    </div>
</div>