<div id="edit_license_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    ×
                </button>
                <h3>Edit industry</h3>
            </div>
            <div class="modal-body">
                <form method="post" class="form-horizontal">
                    <input type="hidden" name="license">
                    <div class="form-group">
                        <label class="control-label col-sm-2">Name</label>
                        <div class="col-sm-10">
                            <input class="form-control" name="name" type="text" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">Fee</label>
                        <div class="col-sm-10">
                            <input class="form-control" name="fee" type="number" min="0" step="0.001" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">Description</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" name="description" type="text"
                                      required></textarea>
                        </div>
                    </div>
                    {{csrf_field()}}
                    <div class="form-group">
                        <div class="col-sm-10 col-sm-offset-9">
                            <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true">
                                Abort
                            </button>
                            <input class="btn btn-danger" type="submit" value="Edit">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>