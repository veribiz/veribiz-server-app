@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-md-offset-0">
                <div class="panel panel-default">
                    <div class="panel-heading">Rejected businesses permit applications</div>
                    <div class="panel-body">
                        @if ( Session::has('msg') )
                            <div class="alert alert-success alert-dismissable">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong>Success!</strong> {{Session::get("msg")}}
                            </div>
                        @endif
                        @if(count($applications) > 0)
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Business</th>
                                    <th>Permit</th>
                                    <th>Duration</th>
                                    <th>User</th>
                                    <th>Rejected on</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($applications as $application)
                                    <tr>
                                        <td> {{ $application->business->name }} </td>
                                        <td> {{ $application->permit->name }} </td>
                                        <td> {{ $application->duration }} months</td>
                                        <td> {{ $application->business->user->name }} </td>
                                        <td> {{ $application->updated_at }} </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {!! $applications->appends(['q' => 'rejected'])->render() !!}
                        @else
                            <div class="alert alert-info">
                                There are no rejected business permit applications!
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection