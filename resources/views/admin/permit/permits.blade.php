@extends('layouts.admin')

@section('content')
    @include('includes.confirm_op')
    @include('includes.edit_permit')
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-md-offset-0">
                <div class="panel panel-default">
                    <div class="panel-heading">Permits</div>
                    <div class="panel-body">
                        @if ( !empty ( $msg ) )
                            <div class="alert alert-success alert-dismissable">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong>Success!</strong> {{$msg}}
                            </div>
                        @endif
                            @if ( Session::has('msg') )
                                <div class="alert alert-success alert-dismissable">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <strong>Success!</strong> {{Session::get("msg")}}
                                </div>
                            @endif
                            @if ( Session::has('fail') )
                                <div class="alert alert-danger alert-dismissable">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <strong>Failure!</strong> {{Session::get("fail")}}
                                </div>
                            @endif
                        @if(count($permits) > 0)
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Fee</th>
                                    <th>Description</th>
                                    <th colspan="2">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($permits as $permit)
                                    <tr>
                                        <td style="display: none">
                                            {{ $permit->id }}
                                        </td>
                                        <td> {{ $permit->name }} </td>
                                        <td> {{ $permit->fee }} </td>
                                        <td> {{ $permit->description}} </td>
                                        <td>
                                            <a class="btn btn-default btn-sm" id="_edit_permit">Edit</a>
                                        </td>
                                        <td>
                                            <a class="btn btn-danger btn-sm"
                                               href="{{url()->current() . '?q=delete&id=' . $permit->id}}"
                                               id="_delete_action_btn">
                                                Delete
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="alert alert-info">
                                There are no permits!
                            </div>
                        @endif
                    </div>
                </div>
                {!! $permits->appends(['q' => 'permits'])->render() !!}
            </div>
        </div>
    </div>
@endsection