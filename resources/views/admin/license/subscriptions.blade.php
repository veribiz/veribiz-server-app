@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-md-offset-0">
                <div class="panel panel-default">
                    <div class="panel-heading">License subscriptions</div>
                    <div class="panel-body">
                        @if(!empty($subscriptions) && count($subscriptions) > 0)
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>License</th>
                                    <th>Business</th>
                                    <th>Fee</th>
                                    <th>Commencement</th>
                                    <th>Expiry</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($subscriptions as $subscription)
                                    <tr>
                                        <td> {{ $subscription->license->name }} </td>
                                        <td> {{ $subscription->business->name }} </td>
                                        <td> {{ $subscription->fee }} </td>
                                        <td> {{ $subscription->commencement }} </td>
                                        <td> {{ $subscription->expiry }} </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {!! $subscriptions->appends(['q' => 'subscriptions'])->render() !!}
                        @else
                            <div class="alert alert-info">
                                There are no license subscriptions!
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection