@extends('layouts.admin')

@section('content')
    @include('includes.confirm_op')
    @include('includes.edit_license')
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-md-offset-0">
                <div class="panel panel-default">
                    <div class="panel-heading">Licenses</div>
                    <div class="panel-body">
                        @if ( !empty ( $msg ) )
                            <div class="alert alert-success alert-dismissable">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong>Success!</strong> {{$msg}}
                            </div>
                        @endif
                            @if ( Session::has('msg') )
                                <div class="alert alert-success alert-dismissable">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <strong>Success!</strong> {{Session::get("msg")}}
                                </div>
                            @endif
                            @if ( Session::has('fail') )
                                <div class="alert alert-danger alert-dismissable">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <strong>Failure!</strong> {{Session::get("fail")}}
                                </div>
                            @endif
                        @if(count($licenses) > 0)
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Fee</th>
                                    <th>Description</th>
                                    <th colspan="2">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($licenses as $license)
                                    <tr>
                                        <td style="display: none">
                                            {{ $license->id }}
                                        </td>
                                        <td> {{ $license->name }} </td>
                                        <td> {{ $license->fee }} </td>
                                        <td> {{ $license->description}} </td>
                                        <td>
                                            <a class="btn btn-default btn-sm" id="_edit_license">Edit</a>
                                        </td>
                                        <td>
                                            <a class="btn btn-danger btn-sm"
                                               href="{{url()->current() . '?q=delete&id=' . $license->id}}"
                                               id="_delete_action_btn">
                                                Delete
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="alert alert-info">
                                There are no licenses!
                            </div>
                        @endif
                    </div>
                </div>
                {!! $licenses->appends(['q' => 'licenses'])->render() !!}
            </div>
        </div>
    </div>
@endsection