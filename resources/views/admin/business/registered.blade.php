@extends('layouts.admin')

@section('content')
    @include('includes.admin_bus_detailed')
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-md-offset-0">
                <div class="panel panel-default">
                    <div class="panel-heading">Registered businesses</div>
                    <div class="panel-body">
                        @if(count($businesses) > 0)
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Phone a</th>
                                    <th>Email</th>
                                    <th>Location</th>
                                    <th>Owner</th>
                                    <th>Industry</th>
                                    <th>Registered on</th>
                                    <th>View</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($businesses as $business)
                                    <tr>
                                        <td> {{ $business->name }} </td>
                                        <td> {{ $business->phone_a }} </td>
                                        <td> {{ $business->email }} </td>
                                        <td> {{ $business->location }} </td>
                                        <td> {{ $business->user->name }} </td>
                                        <td> {{ $business->industry->name }} </td>
                                        <td> {{ $business->created_at }}</td>
                                        <td>
                                            <button class="btn btn-default btn-sm"
                                                    id="_biz_view_detailed"
                                                    value="{{json_encode(
                                                        [
                                                            'name' => $business->name,
                                                            'phone_a' => $business->phone_a,
                                                            'phone_b' => $business->phone_b,
                                                            'email' => $business->email,
                                                            'location' => $business->location,
                                                            'coordinates_d' => $business->lat . ', ' . $business->lng,
                                                            'coordinates' => $business->lat . ',' . $business->lng,
                                                            'description' => $business->description,
                                                            'owner' => $business->user->name,
                                                            'industry' => $business->industry->name,
                                                            'date' =>  $business->created_at->format('d-m-Y H:i:s'),
                                                            'date_title' => 'Applied on'
                                                        ])}}">
                                                Detailed
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {!! $businesses->appends(['q' => 'view'])->render() !!}
                        @else
                            <div class="alert alert-info">
                                There are no registered businesses!
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
