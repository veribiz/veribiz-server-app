@extends('layouts.admin')

@section('content')
    @include('includes.confirm_op')
    @include('includes.admin_bus_detailed')
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-md-offset-0">
                <div class="panel panel-default">
                    <div class="panel-heading">Businesses registration applications</div>
                    <div class="panel-body">
                        @if(count($applications) > 0)
                            <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Business name</th>
                                        <th>Email</th>
                                        <th>Location</th>
                                        <th>Owner</th>
                                        <th>Industry</th>
                                        <th>Applied on</th>
                                        <th>View</th>
                                        <th colspan="2">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($applications as $application)
                                        <tr>
                                            <td> {{ $application->name }} </td>
                                            <td> {{ $application->email }} </td>
                                            <td> {{ $application->location }} </td>
                                            <td> {{ $application->user->name }} </td>
                                            <td> {{ $application->industry->name }} </td>
                                            <td> {{ $application->created_at }} </td>
                                            <td>
                                                <button class="btn btn-default btn-sm"
                                                        id="_biz_view_detailed"
                                                        value="{{json_encode(
                                                        [
                                                            'name' => $application->name,
                                                            'phone_a' => $application->phone_a,
                                                            'phone_b' => $application->phone_b,
                                                            'email' => $application->email,
                                                            'location' => $application->location,
                                                            'coordinates_d' => $application->lat . ', ' . $application->lng,                                                            'coordinates' => $application->lat . ', ' . $application->lng,
                                                            'description' => $application->description,
                                                            'owner' => $application->user->name,
                                                            'industry' => $application->industry->name,
                                                            'date' =>  $application->created_at->format('d-m-Y H:i:s'),
                                                            'date_title' => 'Applied on'
                                                        ])}}"
                                                        >
                                                    Detailed
                                                </button>
                                            </td>
                                            <td>
                                                <a class="btn btn-default btn-sm"
                                                   href="{{url()->current() . '?q=approve&id=' . $application->id}}"
                                                   id="_approve_action_btn">
                                                    Approve
                                                </a>
                                            </td>
                                            <td>
                                                <a class="btn btn-default btn-sm"
                                                   href="{{url()->current() . '?q=reject&id=' . $application->id}}"
                                                   id="_reject_action_btn">
                                                    Reject
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                            </table>
                            {!! $applications->appends(['q' => 'pending'])->render() !!}
                        @else
                            <div class="alert alert-info">
                                There are no business registration applications!
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
