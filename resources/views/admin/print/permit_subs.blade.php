<div class="panel-heading">Permit subscriptions</div>
<div class="panel-body">
    @if(!empty($subscriptions) && count($subscriptions) > 0)
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>Permit</th>
                <th>Business</th>
                <th>Fee</th>
                <th>Commencement</th>
                <th>Expiry</th>
            </tr>
            </thead>
            <tbody>
            @foreach($subscriptions as $subscription)
                <tr>
                    <td> {{ $subscription->permit->name }} </td>
                    <td> {{ $subscription->business->name }} </td>
                    <td> {{ $subscription->fee }} </td>
                    <td> {{ $subscription->commencement }} </td>
                    <td> {{ $subscription->expiry }} </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @else
        <div class="alert alert-info">
            There are no permit subscriptions!
        </div>
    @endif
</div>
