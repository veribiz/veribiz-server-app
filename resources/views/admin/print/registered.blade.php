<div class="panel-heading">Registered businesses</div>
<div class="panel-body">
    @if(count($businesses) > 0)
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>Name</th>
                <th>Phone a</th>
                <th>Email</th>
                <th>Location</th>
                <th>Owner</th>
                <th>Industry</th>
                <th>Registered on</th>
            </tr>
            </thead>
            <tbody>
            @foreach($businesses as $business)
                <tr>
                    <td> {{ $business->name }} </td>
                    <td> {{ $business->phone_a }} </td>
                    <td> {{ $business->email }} </td>
                    <td> {{ $business->location }} </td>
                    <td> {{ $business->user->name }} </td>
                    <td> {{ $business->industry->name }} </td>
                    <td> {{ $business->created_at }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @else
        <div class="alert alert-info">
            There are no registered businesses!
        </div>
    @endif
</div>