$(document).ready(function () {
    $('[id^=_approve_action_btn]').click(function (e) {
        e.preventDefault();
        var href = $(this).attr('href');
        var actionConfirm = $('#action_confirm');
        var module = getCurrentModule(href);
        actionConfirm.find('.modal-body').text(
            'Are you sure you want to approve this ' + module + ' application?'
        );
        var actionOk = $('#action_ok');
        actionOk.text("Approve");
        actionOk.attr('href', href);
        actionConfirm.modal({show: true});
    });

    $('[id^=_reject_action_btn]').click(function (e) {
        e.preventDefault();
        var href = $(this).attr('href');
        var actionConfirm = $('#action_confirm');
        var module = getCurrentModule(href);
        actionConfirm.find('.modal-body').text(
            'Are you sure you want to reject this ' + module + ' application?'
        );
        var actionOk = $('#action_ok');
        actionOk.text("Reject");
        actionOk.attr('href', href);
        actionConfirm.modal({show: true});
    });

    $('[id^=_cancel_action_btn]').click(function (e) {
        e.preventDefault();
        var href = $(this).attr('href');
        var actionConfirm = $('#action_confirm');
        var module = getCurrentModule(href);
        actionConfirm.find('.modal-body').text(
            'Are you sure you want to cancel this ' + module + ' application?'
        );
        var actionOk = $('#action_ok');
        actionOk.text("Cancel");
        actionOk.attr('href', href);
        actionConfirm.modal({show: true});
    });

    var bizSelect = $('#bus_select');

    bizSelect.val($('#initial_biz').val()).change();

    bizSelect.on('change', function() {
        $('#bus_sel_form').trigger('submit');
    });

    $('#detect_gps').click(function () {
        /*
        Try getting coordinates via both android USB-tether or wifi hotspot. Whichever is running will
        give us the coordinates or any other appropriate response. If any times out it means there is
        no such connection to the Android device.
         */
        var infoModal = $('#info_modal');
        var commSuccess = false;
        var sResponse;
        setTimeout(function () {
            //see if it was unsuccessful
            if (!commSuccess)
            {
                infoModal.find('.modal-body').html(
                    'Connection to Veribiz Android App cannot be established. ' +
                    'Ensure you are connected via Wifi Hotspot or USB tethering ' +
                    'and the App is running'
                );
                infoModal.modal({show: true});
            }
            else
            {
                if (!(sResponse.success)){
                    infoModal.find('.modal-body').html(
                        'Veribiz Android App cannot get the location because ' +
                        'GPS is disabled on the device. Enable GPS and restart app.'
                    );
                    infoModal.modal({show: true});
                }
            }
        }, 800);
        var uri = 'get-coordinates';
        $.ajax({
            timeout: 700,
            type: 'GET',
            url: 'http://192.168.43.1:8080/' + uri,//Android default wifi hotspot IP
            success: function(response) {
                commSuccess = true;
                sResponse = response;
            }
        });
        $.ajax({
            timeout: 700,
            type: 'GET',
            url: 'http://192.168.42.129:8080/' + uri,//Android default  USB-tether IP
            success: function(response) {
                commSuccess = true;
                sResponse = response;
            }
        });
        setTimeout(function () {
            if(sResponse.success)
            {
                var latitude = sResponse.latitude;
                var longitude = sResponse.longitude;
                if ( latitude !== 0 &&  longitude !== 0 ){
                    $('#lat').val(latitude);
                    $('#long').val(longitude);
                }
                else
                {
                    infoModal.find('.modal-body').html(
                        'The GPS on the device has not established a location fix. Try again after a while.'
                    );
                    infoModal.modal({show: true});
                }
            }
        }, 900);
    });

    $('[id^=_biz_view_detailed]').click(function (e) {
        e.preventDefault();
        var details = $.parseJSON($(this).val());
        $('#_biz_detail_name').text(details.name);
        var businessDetailsModal = $('#bus_detailed_modal');
        var modalBody = businessDetailsModal.find('.modal-body');
        var tableBodyRow = modalBody.find('tbody').find('tr');
        tableBodyRow.find('#phone_a').text(details.phone_a);
        tableBodyRow.find('#phone_b').text(details.phone_b);
        tableBodyRow.find('#email').text(details.email);
        tableBodyRow.find('#location').text(details.location);
        tableBodyRow.find('#coordinates').text(details.coordinates_d);
        tableBodyRow.find('#go_to_map').attr('href',
            'http://maps.google.com/maps?q=' + details.coordinates
        );
        tableBodyRow.find('#owner').text(details.owner);
        tableBodyRow.find('#description').text(details.description);
        tableBodyRow.find('#industry').text(details.industry);
        tableBodyRow.find('#date').text(details.date);
        tableBodyRow.find('#date_title').text(details.date_title);
        businessDetailsModal.modal({show: true});
    });

    $('[id^=app_biz_view_detailed]').click(function (e) {
        e.preventDefault();
        var details = $.parseJSON($(this).val());
        $('#app_biz_detail_name').text(details.name);
        var businessDetailsModal = $('#app_bus_detailed_modal');
        var modalBody = businessDetailsModal.find('.modal-body');
        var tableBodyRow = modalBody.find('tbody').find('tr');
        tableBodyRow.find('#phone_a').text(details.phone_a);
        tableBodyRow.find('#phone_b').text(details.phone_b);
        tableBodyRow.find('#email').text(details.email);
        tableBodyRow.find('#location').text(details.location);
        tableBodyRow.find('#coordinates').text(details.coordinates_d);
        tableBodyRow.find('#go_to_map').attr('href',
            'http://maps.google.com/maps?q=' + details.coordinates
        );
        tableBodyRow.find('#owner').text(details.owner);
        tableBodyRow.find('#description').text(details.description);
        tableBodyRow.find('#industry').text(details.industry);
        tableBodyRow.find('#date').text(details.date);
        tableBodyRow.find('#date_title').text(details.date_title);
        businessDetailsModal.modal({show: true});
    });

    $.fn.image = function(src, f) {
        return this.each(function() {
            var i = new Image();
            i.src = src;
            i.onload = f;
            this.appendChild(i);
        });
    }

    $('[id^=_show_printout]').click(function (e) {
        e.preventDefault();
        var printoutModal = $('#printout_modal');
        var qrArea = printoutModal.find('#qr_code');
        var busRow = $(this).parent().parent().children();
        printoutModal.find('#bus_name').html(busRow[0].innerHTML);
        qrArea.empty();
        qrArea.image($(this).attr('href'), function () {
            printoutModal.modal({show: true});
        });
    });

    $('#print_reg_cert').click(function (e) {
        $('#print_area').printThis();
    });

    $('[id^=_print]').click(function (e) {
        e.preventDefault();
        var what = $(this).html();
        var baseUrl = $('#base_url').val() + '/print?q=';
        var url = null;
        switch (what)
        {
            case 'Permits':
                url = baseUrl + 'permits';
                break;
            case 'Licenses':
                url = baseUrl + 'licenses';
                break;
            case 'Industries':
                url = baseUrl + 'industries';
                break;
            case 'Registered businesses':
                url = baseUrl + 'registered';
                break;
            case 'License subscriptions':
                url = baseUrl + 'license_subs';
                break;
            case 'Permit subscriptions':
                url = baseUrl + 'permit_subs';
                break;
            default:
                return;
        }
        $.get(url, {}, function (response) {
            var restorePage = document.body.innerHTML;
            document.body.innerHTML = "<div id='pa'>" + response + "</div>";
            setTimeout(function() {
                $('#pa').printThis();
                setTimeout(function() {
                    document.body.innerHTML = restorePage;
                    location.reload();
                }, 800);
            }, 50);
        });
    });

    $('[id^=_edit_industry]').click(function (e) {
        e.preventDefault();
        var row = $(this).parent().parent();
        var data = row.children();
        $("input[name='industry']").val(data[0].innerHTML);
        $("input[name='name']").val(data[1].innerHTML);
        $("textarea[name='description']").val(data[2].innerHTML);
        $('#edit_industry_modal').modal('show');
    });

    $('[id^=_edit_permit]').click(function (e) {
        e.preventDefault();
        var row = $(this).parent().parent();
        var data = row.children();
        $("input[name='permit']").val(data[0].innerHTML);
        $("input[name='name']").val(data[1].innerHTML);
        $("input[name='fee']").val(parseFloat(data[2].innerHTML));
        $("textarea[name='description']").val(data[3].innerHTML);
        $('#edit_permit_modal').modal('show');
    });

    $('[id^=_edit_license]').click(function (e) {
        e.preventDefault();
        var row = $(this).parent().parent();
        var data = row.children();
        $("input[name='license']").val(data[0].innerHTML);
        $("input[name='name']").val(data[1].innerHTML);
        $("input[name='fee']").val(parseFloat(data[2].innerHTML));
        $("textarea[name='description']").val(data[3].innerHTML);
        $('#edit_license_modal').modal('show');
    });

    $('[id^=_delete_action_btn]').click(function (e) {
        e.preventDefault();
        var href = $(this).attr('href');
        var actionConfirm = $('#action_confirm');
        var module = getCurrentModule(href);
        actionConfirm.find('.modal-body').text(
            'Are you sure you want to delete this ' + module + '?'
        );
        var actionOk = $('#action_ok');
        actionOk.text("Delete");
        actionOk.attr('href', href);
        actionConfirm.modal({show: true});
    });
});

function setBiz(bus) {
    alert(bus);
    $('#bus_select').val(bus);
}

function getCurrentModule(href) {
    if(~href.indexOf('business'))
    {
        return 'business';
    }
    else if(~href.indexOf('license'))
    {
        return 'license';
    }
    else if(~href.indexOf('permit'))
    {
        return 'permit';
    }
    else if(~href.indexOf('industry'))
    {
        return 'industry';
    }
    return null;
}